<?php

class Uploader_AmazonS3_Test extends UnitTestCase {
  protected $times = 1;
  protected $ua;

  public function setUp() {
    global $config;
    $c = $config['amazon_s3'];

    // Create uploader.
    $this->uploader = new Cloud_Uploader_AmazonS3(
      $c['key'],
      $c['secret'],
      $c['bucket'],
      $c['url_base']
    );
  }

  public function testUpload() {
    global $path_base;

    $requests = array();
    $file_path = join( DIRECTORY_SEPARATOR, array( $path_base, 'files', 'image.jpg' ) );
    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = new Cloud_Uploader_UploadRequest( 'test/image.jpg', $file_path, 'image/jpeg' );
      $requests[$i] = $request;
    }

    $this->uploader->upload( $requests );

    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = $requests[$i];
      $this->assertNull( $request->exception );
      if ( $request->exception ) $this->dump($request->exception);
    }
  }

  public function testUploadHeaders() {
    global $path_base;

    $test_headers = array(
        'Cache-Control' => 'max-age: 3600, public'
    );

    $requests = array();
    $file_path = join( DIRECTORY_SEPARATOR, array( $path_base, 'files', 'image.jpg' ) );
    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = new Cloud_Uploader_UploadRequest( 'test/image.jpg', $file_path, 'image/jpeg', $test_headers );
      $requests[$i] = $request;
    }

    $this->uploader->upload( $requests );

    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = $requests[$i];
      $this->assertNull( $request->exception );
      if ( $request->exception ) $this->dump($request->exception);

      $http_request = new Cloud_Uploader_HTTPMessage( 'GET', $request->result_url );
      $verifies = $this->uploader->http_user_agent->send( array($http_request) );

      $verify = $verifies[0];
      foreach ( $test_headers as $name => $value ) {
        $lc_name = strtolower($name);
        $this->assertEqual( $verify->headers[$lc_name], $test_headers[$name] );
      }
    }
  }
}

class Uploader_AzureBlob_Test extends Uploader_AmazonS3_Test {
  public function setUp() {
    global $config;
    $c = $config['azure_blob'];

    // Create uploader.
    $this->uploader = new Cloud_Uploader_AzureBlob(
      $c['key'],
      $c['secret'],
      $c['bucket'],
      $c['url_base']
    );
  }
}

class Uploader_AmazonS3_URLDetect_Test extends Uploader_AmazonS3_Test {
  public function setUp() {
    global $config;
    $c = $config['amazon_s3'];

    // Create uploader.
    $this->uploader = new Cloud_Uploader_AmazonS3(
      $c['key'],
      $c['secret'],
      $c['bucket']
    );
  }
}

class Uploader_AzureBlob_URLDetect_Test extends Uploader_AmazonS3_Test {
  public function setUp() {
    global $config;
    $c = $config['azure_blob'];

    // Create uploader.
    $this->uploader = new Cloud_Uploader_AzureBlob(
      $c['key'],
      $c['secret'],
      $c['bucket']
    );
  }
}
