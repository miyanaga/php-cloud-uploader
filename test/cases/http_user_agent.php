<?php

class HTTPUserAgent_cURL_Test extends UnitTestCase {
  protected $times = 2;
  protected $ua;

  public function setUp() {
    // Create cURL user agent.
    $this->ua = new Cloud_Uploader_HTTPUserAgent_cURL;
  }

  public function testGet() {
    global $echo_url;

    // Request to echo.php with get paramater.
    $requests = array();
    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = new Cloud_Uploader_HTTPMessage( 'GET', $echo_url . '?index=' . $i, array( 'x-header-value' => $i ) );
      $requests[$i] = $request;
    }

    $responses = $this->ua->send($requests);

    for ( $i = 0; $i < $this->times; $i++ ) {
      $result = unserialize($responses[$i]->body);

      // Test get parameter echoed in headers and body.
      $this->assertEqual( $result['_SERVER']['REQUEST_METHOD'], 'GET' );
      $this->assertEqual( $responses[$i]->headers['x-echo-get-index'], $i, "Matching header from get parameter of reasponse[$i]" );
      $this->assertEqual( $result['_GET']['index'], $i, "Matching body of response[$i]" );
      $this->assertEqual( $result['_SERVER']['HTTP_X_HEADER_VALUE'], $i, "Matching request header of response[$i]" );
    }
  }

  public function testPut() {
    global $echo_url;
    global $path_base;

    $requests = array();
    $file_path = join( DIRECTORY_SEPARATOR, array( $path_base, 'files', 'image.jpg' ) );
    for ( $i = 0; $i < $this->times; $i++ ) {
      $request = new Cloud_Uploader_HTTPMessage( 'PUT', $echo_url, null,  $file_path );
      $requests[$i] = $request;
    }

    $responses = $this->ua->send($requests);

    for ( $i = 0; $i < $this->times; $i++ ) {
      $result = unserialize($responses[$i]->body);
      $this->assertEqual( $result['_SERVER']['REQUEST_METHOD'], 'PUT' );
      $this->assertEqual( strlen($result['_BODY']), filesize($file_path), "Matching file size of response[$i]" );

      $body_md5 = md5($result['_BODY']);
      $file_md5 = md5(file_get_contents($file_path));
      $this->assertEqual( $body_md5, $file_md5, "Mactching md5 of response[$i]" );
    }
  }

  public function testErrors() {
    global $echo_url;
    global $error_url;

    $requests = array(
      'bad_url' => new Cloud_Uploader_HTTPMessage( 'GET', 'http://.' ),
      'post' => new Cloud_Uploader_HTTPMessage( 'POST', $echo_url ),
      '404' => new Cloud_Uploader_HTTPMessage( 'GET', $echo_url . '-not-found' ),
      '500' => new Cloud_Uploader_HTTPMessage( 'GET', $error_url . '?status=' . urlencode('500 Internal Server Error') ),
    );

    $responses = $this->ua->send($requests);

    // Bad URL.
    $res = $responses['bad_url'];
    $this->assertTrue( $res->exception );
    if ( $res->exception ) {
      $this->assertEqual( $res->exception->getCode(), Cloud_Uploader_Exception::http_connection_error );
    }

    // Post.
    $res = $responses['post'];
    $this->assertTrue( $res->exception );
    if ( $res->exception ) {
      $this->assertEqual( $res->exception->getCode(), Cloud_Uploader_Exception::parameter_violation );
    }

    // 404
    $res = $responses['404'];
    $this->assertTrue( $res->exception );
    if ( $res->exception ) {
      $this->assertEqual( $res->exception->getCode(), Cloud_Uploader_Exception::http_response_error );
      $this->assertEqual( $res->exception->extra_code, 404 );
    }

    // 500
    $res = $responses['500'];
    $this->assertTrue( $res->exception );
    if ( $res->exception ) {
      $this->assertEqual( $res->exception->getCode(), Cloud_Uploader_Exception::http_response_error );
      $this->assertEqual( $res->exception->extra_code, 500 );
    }
  }
}

class HTTPUserAgent_HTTPRequest_Test extends HTTPUserAgent_cURL_Test {
  public function setUp() {
    // Create HTTP_Request user agent.
    $this->ua = new Cloud_Uploader_HTTPUserAgent_HTTPRequest;
  }

}
