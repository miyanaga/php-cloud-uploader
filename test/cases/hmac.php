<?php

class HMAC_builtin_Test extends UnitTestCase {
  protected $hmac;

  public function setUp() {
    $this->hmac = new Cloud_Uploader_HMAC_builtin;
  }

  public function testHASH() {
    $dummy_key = 'abcdef';
    $dummy_data = 'Hello world';
    $expects = array(
      'md5'     => 'UbmHjn9QtJ5osda2rwrsUA==',
      'sha1'    => '+9Z6AA7pDnlj9zo44jPCr/x7A3g=',
      'sha256'  => 'k0+GyYw/IbWHmurQGrBtLBfje0RVyBbpg+9t7nyL0/8=',
    );

    foreach ( $expects as $algorithm => $expected ) {
      $value = base64_encode( $this->hmac->hash( $algorithm, $dummy_data, $dummy_key ) );
      $this->assertEqual( $value, $expected );
    }

  }
}

class HMAC_Crypt_HMAC2_Test extends HMAC_builtin_Test {
  public function setUp() {
    $this->hmac = new Cloud_Uploader_HMAC_CryptHMAC2;
  }

}
