<?php

// Echo back serialized request variables to test.

$values = array(
  '_SERVER' => $_SERVER,
  '_GET'    => $_GET,
  '_POST'   => $_POST,
  '_REQUEST' => $_REQUEST,
  '_COOKIE' => $_COOKIE,
  '_FILES'  => $_FILES,
  '_BODY'    => file_get_contents("php://input")
);

// Echo get param as header.
foreach ( $_GET as $name => $value ) {
  header("x-echo-get-$name: $value");
}

// file_put_contents( 'echo-last-result.txt', var_export( $values, true ) );

echo serialize($values);
