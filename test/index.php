<?php

// Use simpletest library.
require_once('simpletest/unit_tester.php');
require_once('../lib/Cloud/Uploader.php');

// Create test suite.
$suite = new TestSuite();
$suite->TestSuite('Cloud Uploader Tests');

// Load cases.
$cases = './cases';
$dh = opendir($cases);
while ( false !== ($file = readdir($dh)) ) {
  if ( substr( $file, -4 ) == '.php' ) {
    $suite->addFile("$cases/$file");
  }
}

// URL base.
$url_base = preg_replace( '!/[^/]*$!', '', $_SERVER['SCRIPT_URI'] );
$echo_url = "$url_base/echo.php";
$error_url = "$url_base/error.php";

// Path base.
$sep = DIRECTORY_SEPARATOR;
$path_base = preg_replace( '!' . $sep . '[^' . $sep . ']*$!', '', __FILE__ );

// Load config.
$config = array();
$config_file = './test-config.php';
if ( file_exists( $config_file ) ) {
  include( $config_file );
}

// Run the test suite.
$suite->run(new HtmlReporter());
