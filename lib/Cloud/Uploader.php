<?php

/**
 * Cloud Uploader Library.
 *
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 * @copyright Copyright (c) 2011, ideaman's Inc. Kunihiko Miyanaga
 * @version 0.8.0
 * @license The MIT License
 */

/**
 * Utility functions class.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_Util {
  /**
   * Catenate path partials.
   *
   * Normalize not to repeat separators.
   *
   * @param string $glue Glue to join partials.
   * @param string $separator Canditate of separator characters.
   * @param string Normalized and joined path.
   */
  public static function cat_path( $glue, $separator, $paths ) {
    for ( $i = 1; $i < count($paths); $i++ ) {
      $paths[$i] = preg_replace( '!^[' . $separator . ']+!', '', $paths[$i] );
    }
    for ( $i = 0; $i < count($paths) - 1; $i++ ) {
      $paths[$i] = preg_replace( '![' . $separator . ']+$!', '', $paths[$i] );
    }
    return join( $glue, $paths );
  }

  /**
   * Catenate url.
   *
   * @param variable URL partials as string.
   * @param return Catenated URL.
   */
  public static function cat_url() {
    $args = func_get_args();
    return self::cat_path( '/', '/', $args );
  }

  /**
   * Catenate directory path.
   *
   * @param variable Directory path partials as string.
   * @param return Catenated directory path.
   */
  public static function cat_dir() {
    $args = func_get_args();
    $separator = DIRECTORY_SEPARATOR;

    // Concider separator mixed directory path on windows.
    if ( $separator != '/' ) $separator .= '/';

    return self::cat_path( DIRECTORY_SEPARATOR, $separator, $args );
  }
}

/**
 * Cloud uploader exception class.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_Exception extends Exception {
  /**
   * Related HTTP response.
   */
  public $http_response = null;

  /**
   * Extra error code.
   *
   * Error code from other library like cURL.
   */
  public $extra_code = null;

  /**
   * Object access error.
   *
   * Example:
   * - The class of the object is to be a base and required to inherit.
   * - The object requires undefined function or class.
   */
  const object_access_error = 0x0001;

  /**
   * Parameter violation.
   *
   * Passed parameter not allowed or not supported.
   */
  const parameter_violation = 0x0002;

  /**
   * HTTP connection error.
   */
  const http_connection_error = 0x0003;

  /**
   * HTTP response error.
   *
   * Response status code is not between 200 and 299.
   */
  const http_response_error = 0x0004;

  /**
   * Mismatch in verification.
   */
  const verification_mismatch = 0x0005;

  /*
   * Constructor.
   *
   * @param string $message Message.
   * @param integer $code Error code.
   * @param Cloud_Uploader_HTTPMessage $http_response Related HTTP response.
   */
  public function __construct( $message, $code, $http_response = null ) {
    parent::__construct( $message, $code );
    $this->http_response = $http_response;
  }
}

/**
 * HTTP request and response message.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_HTTPMessage {
  /**
   * Request method.
   */
  public $method = 'GET';

  /**
   * Request URL.
   */
  public $url;

  /**
   * Status code of response.
   */
  public $status_code;

  /**
   * Status reason of response.
   */
  public $status_reason;

  /**
   * Request or response headers.
   */
  public $headers = array();

  /**
   * Request of response body.
   */
  public $body;

  /**
   * Common options like timeout.
   */
  public $common_options = array();

  /**
   * Additional options by library.
   */
  public $library_options = array();

  /**
   * Companion message. Response to the request, and request to the response.
   */
  public $companion;

  /**
   * File handle. Only used in PUT method in cURL.
   */
  public $file_handle;

  /**
   * Exception.
   */
  public $exception;

  /**
   * Constructor to initialize message.
   *
   * @param string $method Method using to request.
   * @param string $url URL using to request.
   * @param array $headers HTTP message headers.
   * @param mixed $body HTTP message body. To request, string or fields array. If the method is PUT, set a file path.
   * @param array $common_options Common options for HTTP user agent.
   * @param array $library_options Additional options by HTTP user agent library.
   */
  public function __construct( $method = null, $url = null, $headers = array(), $body = null, $common_options = null, $library_options = null ) {
    // Set object properties from params.
    $props = array( 'method', 'url', 'headers', 'body', 'common_options', 'library_options' );
    foreach ( $props as $prop ) {
      if ( isset($$prop) ) $this->$prop = $$prop;
    }
  }

  /**
   * Pair request and response each other.
   *
   * @param Cloud_Uploader_HTTPMessage $companion Companion message.
   */
  public function pair( $companion ) {
    $this->companion = $companion;
    $companion->companion = $this;
  }
}

/**
 * HTTP User Agent.
 *
 * Abstructs HTTP communication by library. You must use extended class.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_HTTPUserAgent {
  /**
   * Common options.
   */
  public $common_options = array();

  /**
   * Library options.
   */
  public $library_options = array();

  /**
   * Constructor.
   */
  public function __construct( $common_options = null, $library_options = null ) {
    // Common options.
    if ( isset($common_options) ) {
      $this->common_options = $common_options;
    }

    // Library options.
    if ( isset($library_options) ) {
      $this->library_options = $library_options;
    }

    // Default options.
    if ( !isset($this->common_options['timeout']) ) {
      $this->common_options['timeout'] = 300;
    }
  }

  /**
   * Request timeout in second.
   */
  public function timeout() {
    return $this->common_options['timeout'];
  }

  /**
   * Send HTTP requests and get responses with Cloud_Uploader_HTTPMessage.
   *
   * @param array Request messages of Cloud_Uploader_HTTPMessage object.
   * @return array Response messages of Clound_Uploader_HTTPMessage object. Indexes must be corresponding to requests.
   */
  public function send( $requests ) {
    throw new Cloud_Uploader_Exception(
      'Do not use bare Cloud_Uploader_HTTPUserAgent object.',
      Cloud_Uploader_Exception::object_access_error
    );
  }
}

/**
 * Implementation of Cloud_Uploader_HTTPUserAgent with cURL.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_HTTPUserAgent_cURL extends Cloud_Uploader_HTTPUserAgent {
  /**
   * Constructor.
   *
   * @param array $common_options Common options.
   * @param array $library_options Additional options by library.
   */
  public function __construct( $common_options = null, $library_options = null ) {
    // Check if cURL enabled.
    if ( !function_exists('curl_init') ) {
      throw new Cloud_Uploader_Exception(
        'cURL feature is not enabled.',
        Cloud_Uploader_Exception::object_access_error
      );
    }

    // Call parent constructor.
    parent::__construct( $common_options, $library_options );

    // Default options.
    if ( !isset($this->common_options['parallels']) ) {
      $this->common_options['parallels'] = 8;
    }
  }

  /**
   * Number of parallels to request.
   */
  public function parallels() {
    return $this->common_options['parallels'];
  }

  /**
   * Setting up request curl handle.
   *
   * @param Cloud_Uploader_HTTPMessage $request Request message.
   * @param Cloud_Uploader_HTTPMessage $response Response message.
   * @return resource Handle of cURL.
   */
  protected function curl_handle( $request, $response ) {
    // Initialize handle.
    $ch = curl_init( $request->url );

    // Basic options.
    $headers = array();
    foreach ( $request->headers as $name => $value ) {
      $headers []= "$name: $value";
    }
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_BINARYTRANSFER, true );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
    curl_setopt( $ch, CURLOPT_HEADER, true );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch, CURLOPT_MUTE, true );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $this->timeout() );

    // By Request method.
    $lc_method = strtolower($request->method);
    if ( $lc_method == 'put' ) {
      curl_setopt( $ch, CURLOPT_PUT, true );
      $request->file_handle = fopen( $request->body, 'rb' );
      curl_setopt( $ch, CURLOPT_INFILE, $request->file_handle );
      curl_setopt( $ch, CURLOPT_INFILESIZE, filesize($request->body) );
    } else if ( $lc_method != 'get' ) {
      throw new Cloud_Uploader_Exception(
        'Cloud Uploader supports only GET and PUT.',
        Cloud_Uploader_Exception::parameter_violation,
        $response
      );
    }

    // Apply object options.
    foreach ( $this->library_options as $opt => $value ) {
      curl_setopt( $ch, $opt, $value );
    }

    // Apply request options.
    foreach ( $request->library_options as $opt => $value ) {
      curl_setopt( $ch, $opt, $value );
    }

    return $ch;
  }

  /**
   * Build result message from curl.
   *
   * @param resource $ch Handler of cURL.
   * @param Cloud_Uploader_HTTPMessage $request Request message.
   * @param Cloud_Uploader_HTTPMessage $response Response message.
   */
  public function build_response( $ch, $request, $response ) {
    // Get result.
    $exec_result = curl_multi_getcontent($ch);

    // Close file handle if opened.
    if ( $request->file_handle ) {
      fclose( $request->file_handle );
      $request->file_handle = null;
    }

    // Parse full response.
    $status_code = 200;
    $status_reason = null;
    $headers = array();
    $body = $exec_result;

    // Loop to skip 100 continue.
    do {
      // Split to headers and body block.
      $message_blocks = preg_split( '!\r?\n\r?\n!', $body, 2 );
      $headers_block = array_shift( $message_blocks );
      $body = array_shift( $message_blocks );

      // Parse headers block.
      $header_lines = preg_split( '!\r?\n!', $headers_block );
      $status_line = array_shift( $header_lines );

      // Status.
      $statuses = preg_split( '!\s+!', $status_line, 3 );
      array_shift( $statuses ); // Skip protocol.
      $status_code = (int)array_shift( $statuses );
      if ( $status_code == 100 ) continue;
      $status_reason = array_shift( $statuses );

      // Split headers as array.
      foreach ( $header_lines as $line ) {
        $tupple = preg_split( '/\s*:\s*/', $line, 2 );
        if ( count($tupple) > 1 ) {
          $headers[strtolower($tupple[0])] = $tupple[1];
        }
      }
      // Repeat while continue.
    } while ( $status_code == 100 );

    // Create a response message.
    $response->status_code = $status_code;
    $response->status_reason = $status_reason;
    $response->headers = $headers;
    $response->body = $body;
  }

  /**
   * Override send method.
   *
   * @param array $requests Array of Cloud_Uploader_HTTPMessage object to request.
   */
  public function send( $requests ) {
    // cURL can connect in parallel.

    // Devide to chunks keeping array index.
    $chunks = array_chunk( $requests, $this->parallels(), true );
    $responses = array();

    foreach ( $chunks as $chunk ) {
      // Setup multi handle.
      $mh = curl_multi_init();

      // Add each request cURL handle.
      $hundles = array();
      foreach ( $chunk as $index => $request ) {
        $response = new Cloud_Uploader_HTTPMessage( $request->method, $request->url );
        $response->pair($request);
        $responses[$index] = $response;

        try {
          // Initialize curl handle.
          $ch = $this->curl_handle( $request, $response );

          // Add to multi handle.
          curl_multi_add_handle( $mh, $ch );
          $handles[$index] = $ch;
        } catch( Exception $exception ) {
          $response->exception = $exception;
        }
      }

      // Execute.
      try {
        $active = null;
        do {
          $mrc = curl_multi_exec( $mh, $active );
        } while ( $mrc == CURLM_CALL_MULTI_PERFORM );

        while ( $active && $mrc == CURLM_OK ) {
          if ( curl_multi_select($mh) != -1 ) {
            do {
              $mrc = curl_multi_exec( $mh, $active );
            } while ( $mrc == CURLM_CALL_MULTI_PERFORM );
          }
        }
      } catch ( Exception $ex ) {
        // Ignore exception.
      }

      // Get response and remove handles.
      foreach ( $handles as $index => $ch ) {
        try {
          $request = $requests[$index];
          $response = $responses[$index];

          // Connection error.
          $curl_error = curl_error($ch);
          if ( $curl_error ) {
            $exception = new Cloud_Uploader_Exception(
              $curl_error,
              Cloud_Uploader_Exception::http_connection_error,
              $response
            );
            $exception->extra_code = curl_errno($ch);
            throw $exception;
          }

          // Build response from result.
          $this->build_response( $ch, $request, $response );

          // Response error.
          if ( $response->status_code < 200 || $response->status_code >= 300 ) {
            $exception = new Cloud_Uploader_Exception(
              $response->status_reason,
              Cloud_Uploader_Exception::http_response_error,
              $response
            );
            $exception->extra_code = $response->status_code;
            throw $exception;
          }
        } catch ( Exception $exception ) {
          $response->exception = $exception;
        }

        curl_multi_remove_handle( $mh, $ch );
      }

      curl_multi_close($mh);
    }

    return $responses;
  }
}

/**
 * HTTPUserAgent implementation with Pear HTTP_Request.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_HTTPUserAgent_HTTPRequest extends Cloud_Uploader_HTTPUserAgent {
  /**
   * Constructor.
   *
   * @param array $common_options Common options.
   * @param array $library_options Additional options by library.
   */
  public function __construct( $common_options = null, $library_options = null ) {
    // Require the library.
    require_once('HTTP/Request.php');

    parent::__construct( $common_options, $library_options );
  }

  /**
   * Override send method.
   */
  public function send( $requests ) {
    $responses = array();

    foreach ( $requests as $index => $request ) {
      $response = new Cloud_Uploader_HTTPMessage( $request->method, $request->url );
      $response->pair($request);
      $responses[$index] = $response;

      try {
        // Build options array.
        $options = array(
          'method'          => $request->method,
          'timeout'         => $this->timeout(),
          'allowRedirects'  => true,
          'maxRedirects'    => 10,
        );

        // Override library options.
        $options = array_merge( $options, $this->library_options );
        $options = array_merge( $options, $request->library_options );

        // Send request.
        $http = new HTTP_Request( $request->url, $options );

        // Headers.
        foreach ( $request->headers as $name => $value ) {
          $http->addHeader( $name, $value );
        }

        // Body.
        $lc_method = strtolower($request->method);
        if ( $lc_method == 'put' ) {
          $http->setBody( file_get_contents($request->body) );
        } else if ( $lc_method != 'get' ) {
          throw new Cloud_Uploader_Exception(
            'Cloud Uploader supports only GET and PUT.',
            Cloud_Uploader_Exception::parameter_violation,
            $response
          );
        }

        // Send request.
        $res = $http->sendRequest(true);

        // Handle error.
        if ( PEAR::isError($res) ) {
          $error = $res->message;
          if ( !$error ) {
            $error = 'HTTP connectin error.';
          }

          // Exception.
          $exception = new Cloud_Uploader_Exception(
            $error,
            Cloud_Uploader_Exception::http_connection_error,
            $response
          );
          $exception->extra_code = $res->code;
          throw $exception;
        }

        // Build response.
        $response->status_code = $http->getResponseCode();
        $response->status_reason = $http->getResponseReason();
        $response->headers = $http->getResponseHeader();
        $response->body = $http->getResponseBody();

        // Response error.
        if ( $response->status_code < 200 || $response->status_code >= 300 ) {
          $exception = new Cloud_Uploader_Exception(
            $response->status_reason,
            Cloud_Uploader_Exception::http_response_error
          );
          $exception->extra_code = $response->status_code;
          throw $exception;
        }
      } catch ( Exception $exception ) {
        $response->exception = $exception;
      }
    }

    return $responses;
  }
}

/**
 * Abstructs HMAC hashing.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
interface Cloud_Uploader_HMAC {
  /**
   * Make HMAC from data and key.
   *
   * @param string $algorithm Algorithm like md5, sha1 or shaa256.
   * @param string $data Data to make HMAC.
   * @param string $key Key to make HMAC.
   */
  public function hash( $algorithm, $data, $key );
}

/**
 * Implementation of Cloud_Uploader_HMAC with built-in function: hash_hmac().
 */
class Cloud_Uploader_HMAC_builtin implements Cloud_Uploader_HMAC {
  public function __construct() {
    // Check if hash_hmac function enabled.
    if ( !function_exists( 'hash_hmac' ) ) {
      throw new Cloud_Uploader_Exception(
        'Built in function hash_hmac(PHP 5 >= 5.1.2, PECL hash >= 1.1) is not enabled.',
        Cloud_Uploader_Exception::object_access_error
      );
    }
  }

  /**
   * Implements hash method.
   */
  public function hash( $algorithm, $data, $key ) {
    return hash_hmac( $algorithm, $data, $key, true );
  }
}

/**
 * Implementation of Cloud_Uploader_HMAC with Pear Crypt_HMAC2.
 */
class Cloud_Uploader_HMAC_CryptHMAC2 implements Cloud_Uploader_HMAC {
  public function __construct() {
    // Require Crypt_HMAC2.
    require_once('Crypt/HMAC2.php');
  }

  /**
   * Implements hash method.
   */
  public function hash( $algorithm, $data, $key ) {
    $hmac = new Crypt_HMAC2( $key, $algorithm );
    return $hmac->hash( $data, Crypt_HMAC2::BINARY );
  }
}

/**
 * Upload request.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_UploadRequest {
  /**
   * URL path from base.
   */
  public $url_path;

  /**
   * Full path of uploading file.
   */
  public $file_path;

  /**
   * Content type of uploading file.
   */
  public $content_type;

  /**
   * Content length of uploading file.
   */
  public $content_length;

  /**
   * Content md5 of uploading file.
   */
  public $content_md5;

  /**
   * Other HTTP headers.
   */
  public $http_headers = array();

  /**
   * ACL of uploading file. Currently used only for Amazon S3.
   */
  public $acl = 'public-read';

  /**
   * Common options for HTTP request.
   */
  public $http_common_options = array();

  /**
   * Additional options for HTTP request by user agent library.
   */
  public $http_library_options = array();

  /**
   * HTTP request used to upload.
   */
  public $http_request;

  /**
   * HTTP response gotten by uploading.
   */
  public $http_response;

  /**
   * Result url.
   */
  public $result_url;

  /**
   * Signature before hashing.
   */
  public $raw_signature;

  /**
   * Signature after hashing.
   */
  public $signature;

  /**
   * Exception.
   */
  public $exception;

  /**
   * Constructor.
   *
   * @param string $url_path Relative path of url from base.
   * @param string $file_path Absolute path of file to upload.
   * @param string $content_type Content type fo uploading file.
   */
  public function __construct( $url_path, $file_path, $content_type, $http_headers = null ) {
    // Require $url_path.
    if ( !$url_path ) {
      throw new Cloud_Uploader_Exception(
        'Cloud_Uploader_UploadRequest requires $url_path.',
        Cloud_Uploader_Exception::parameter_violation
      );
    }

    // Require $file_path.
    if ( !$file_path || !file_exists($file_path) ) {
      throw new Cloud_Uploader_Exception(
        'Cloud_Uploader_UploadRequest requires existing $file_path.',
        Cloud_Uploader_Exception::parameter_violation
      );
    }

    // Default content-type.
    if ( !$content_type ) {
      $content_type = 'application/octet-stream';
    }

    $this->url_path = $url_path;
    $this->file_path = $file_path;
    $this->content_type = $content_type;
    if ( is_array($http_headers) ) {
      $this->http_headers = $http_headers;
    }

    $this->content_length = filesize($file_path);
    $this->content_md5 = md5_file($file_path);
  }

  /**
   * Verify with HTTP resonse.
   *
   * @param Cloud_Uploader_HTTPMessage HTTP response of GET uploaded file.
   */
  public function verify( $response ) {
    // Content type.
    if ( $this->content_type != $response->headers['content-type'] ) {
      throw new Cloud_Uploader_Exception(
        'Content type mismatch.',
        Cloud_Uploader_Exception::verification_mismatch
      );
    }

    // Content length.
    if ( $this->content_length != strlen($response->body) ) {
      throw new Cloud_Uploader_Exception(
        'Content length mismatch.',
        Cloud_Uploader_Exception::verification_mismatch
      );
    }

    // Content MD5.
    if ( $this->content_md5 != md5($response->body) ) {
      throw new Cloud_Uploader_Exception(
        'Content md5 mismatch.',
        Cloud_Uploader_Exception::verification_mismatch
      );
    }
  }
}

/**
 * Uploader base class.
 *
 * Do not use an instance of thie class.
 * Use each implementation by service such as Cloud_Uploader_AmazonS3.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader {
  /**
   * Package prefix use to generate helper object.
   */
  const package_prefix = 'Cloud_Uploader_';

  /**
   * Base URL to access uploaded file.
   */
  public $url_base;

  /**
   * HTTP user agent helper object.
   */
  public $http_user_agent;

  /**
   * Common options for HTTP user agent.
   */
  public $http_common_options = array();

  /**
   * Additional options for HTTP user agent by library.
   */
  public $http_library_options = array();

  /**
   * Constructor.
   *
   * @param string $url_base URL base of uploaded file.
   * @param mixed $http_user_agent Class suffix of Cloud_Uploader_HTTPUserAgent descendant or the instance.
   */
  public function __construct( $url_base = null, $http_user_agent = null ) {
    // HTTP user agent.
    if ( !$http_user_agent ) {
      // Detection.
      $http_user_agent = function_exists('curl_init')? 'cURL': 'HTTPRequest';
    }
    if ( !is_object($http_user_agent) ) {
      $class = self::package_prefix . 'HTTPUserAgent_' . $http_user_agent;
      $http_user_agent = new $class;
    }

    $this->url_base = $url_base;
    $this->http_user_agent = $http_user_agent;
  }

  /**
   * Get url_base.
   *
   * @return string URL base.
   */
  public function url_base() {
    if ( !$this->url_base ) {
      $this->url_base = $this->default_url_base();
    }
    return $this->url_base;
  }

  /**
   * Get default url_base.
   *
   * Override by the service.
   *
   * @return string Default URL base.
   */
  public function default_url_base() {
    throw new Cloud_Uploader_Exception( 'Do not call default_url_base of bare Cloud_Uploader.' );
  }

  /**
   * Create new request object.
   *
   * @param string $url_path URL path from $url_base.
   * @param string $file_path Full path of uploading file.
   * @param string $content_type Content-Type of uploading file.
   * @param array $http_request HTTP headers option of uploading.
   */
  public function new_request( $url_path, $file_path, $content_type, $http_headers = null ) {
    $request = new Cloud_Uploader_UploadRequest( $url_path, $file_path, $content_type, $http_headers );
    return $request;
  }

  /**
   * Upload file to cloud strage.
   *
   * @param Cloud_Uploader_UploadRequest $request Uploading request.
   * @param boolean $verify Verify uploaded file. Default is true.
   */
  public function upload( $requests, $verify = true ) {
    throw new Cloud_Uploader_Exception(
      'Do not use bare Cloud_Uploader object.',
      Cloud_Uploader_Exception::object_access_error
    );
  }

  /**
   * Verify upload requests.
   */
  public function verify( $requests ) {
    $http_requests = array();
    foreach ( $requests as $index => $request ) {
      // Make URL.
      if ( !$request->result_url ) {
        $request->exception = new Cloud_Uploader_Exception( 'Request has no result_url.' );
        continue;
      }

      // Make request.
      $http_request = new Cloud_Uploader_HTTPMessage( 'GET', $request->result_url );
      $http_requests[$index] = $http_request;
    }

    // Send request.
    $http_responses = $this->http_user_agent->send($http_requests);

    // Verify response.
    foreach ( $requests as $index => $request ) {
      // Skip errored request.
      if ( $request->exception ) continue;

      try {
        // Raise HTTP response exception;
        $http_response = $http_responses[$index];
        if ( $http_response->exception ) {
          throw $http_response->exception;
        }

        $request->verify( $http_responses[$index] );
      } catch ( Exception $exception ) {
        $request->exception = $exception;
      }
    }

  }
}

/**
 * Uploader implementation for Amazon S3 storage.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_AmazonS3 extends Cloud_Uploader {
  /**
   * Cloud storage key/account.
   */
  public $key;

  /**
   * Cloud storage secret phrase.
   */
  public $secret;

  /**
   * Bucket name in the storage account.
   */
  public $bucket;

  /**
   * HMAC helper object.
   */
  public $hmac;

  /**
   * Protocol.
   */
  public $protocol = 'https';

  /**
   * Current timestamp.
   */
  protected $timestamp;

  /**
   * Key name in authentication header.
   */
  protected $auth_prefix = 'AWS';

  /**
   * Prefix of canonical header name.
   */
  protected $header_prefix = 'x-amz-';

  /**
   * Secret is required to decode by base64?
   */
  protected $to_decode_secret = false;

  /**
   * Hash algorithm to sign HMAC.
   */
  protected $hash_algorithm = 'sha1';

  /**
   * Storage domain suffix.
   */
  protected $storage_domain = 's3.amazonaws.com';

  /**
   * Constructor.
   *
   * @param string $key Key/Account name of cloud storage.
   * @param string $secret Secret phrase of cloud storage.
   * @param string $url_base Base URL to get uploaded file. It's OK if CDN root.
   * @param mixed $http_user_agent HTTPUserAgent class name suffix as string, or an instance of the class.
   * @param mixed $hmac HMAC class name suffix as string, or an instance of the class.
   */
  public function __construct( $key, $secret, $bucket, $url_base = null, $http_user_agent = null, $hmac = null ) {
    parent::__construct( $url_base, $http_user_agent );

    // Require $key.
    if ( !$key ) {
      throw new Cloud_Uploader_Exception(
        'Cloud_Uploader requires $key.',
        Cloud_Uploader_Exception::parameter_violation
      );
    }

    // Require $secret.
    if ( !$secret ) {
      throw new Cloud_Uploader_Exception(
        'Cloud_Uploader requires $secret.',
        Cloud_Uploader_Exception::parameter_violation
      );
    }

    // Require $bucket.
    if ( !$bucket ) {
      throw new Cloud_Uploader_Exception(
        'Cloud_Uploader requires $bucket.',
        Cloud_Uploader_Exception::parameter_violation
      );
    }

    // HMAC object.
    if ( !$hmac ) {
      // Detection.
      $hmac = function_exists('hash_hmac')? 'builtin': 'CryptHMAC2';
    }
    if ( !is_object($hmac) ) {
      $class = self::package_prefix . 'HMAC_' . $hmac;
      $hmac = new $class;
    }

    $this->key = $key;
    $this->secret = $secret;
    $this->bucket = $bucket;
    $this->hmac = $hmac;

    $this->update_timestamp();
  }

  /**
   * Override default_url_base.
   */
  public function default_url_base() {
    return sprintf( 'http://%s.s3.amazonaws.com/', $this->bucket );
  }

  /**
   * Update timestamp.
   */
  public function update_timestamp() {
    $this->timestamp = time();
  }

  /**
   * Format current timestamp as GMT.
   */
  public function format_timestamp() {
    return gmdate( 'D, d M Y H:i:s \G\M\T', $this->timestamp );
  }

  /**
   * Get URL to upload.
   */
  public function upload_url( $request ) {
    $url = sprintf( '%s://%s.%s', $this->protocol, $this->bucket, $this->storage_domain );
    $url = Cloud_Uploader_Util::cat_url( $url, $request->url_path );
    return $url;
  }

  /**
   * Make common HTTP request headers.
   */
  public function common_headers( $request ) {
    $headers = array(
      'content-type'    => $request->content_type,
      'content-length'  => $request->content_length
    );
    return $headers;
  }

  /**
   * Make canonical HTTP request headers.
   */
  public function canonical_headers( $request ) {
    $headers = array(
      'x-amz-date'    => $this->format_timestamp(),
      'x-amz-acl'     => $request->acl
    );
    return $headers;
  }

  /**
   * Build signature partial before headers.
   */
  public function signature_before_headers( &$signature, $request ) {
    $signature []= 'PUT';
    $signature []= ''; // $request->content_md5;
    $signature []= $request->content_type;
    $signature []= ''; // $this->format_timestamp();
  }

  /**
   * Build signature canonical headers.
   */
  public function signature_canonical_headers( &$signature, $request, $canonical_headers ) {
    ksort( $canonical_headers );
    foreach ( $canonical_headers as $name => $value ) {
      $name = strtolower($name);
      $value = rawurldecode($value);
      $signature []= "$name:$value";
    }
  }

  /**
   * Build signature canonical resource.
   */
  public function signature_canonical_resource( &$signature, $request ) {
    $path = Cloud_Uploader_Util::cat_url( $this->bucket, $request->url_path );
    $signature []= '/' . $path;
  }

  /**
   * Make signature to request.
   *
   * @param Cloud_Uploader_UploadRequest $request Uploading request.
   * @param array $canonical_headers Headers to sign.
   */
  public function sign( $request, $canonical_headers ) {
    // Prepare signature.
    $signature = array();
    $this->signature_before_headers( $signature, $request );
    $this->signature_canonical_headers( $signature, $request, $canonical_headers );
    $this->signature_canonical_resource( $signature, $request );

    // Normalize signature to join as string.
    foreach ( $signature as $index => $value ) {
      if ( !isset($value) ) $signature[$index] = '';
    }

    // Hash by secret.
    $key = $this->secret;
    if ( $this->to_decode_secret ) {
      $key = base64_decode($key);
    }
    $hashing = join( "\n", $signature );
    $hash = base64_encode( $this->hmac->hash( $this->hash_algorithm, $hashing, $key ) );

    // Keep in request.
    $request->raw_signature = $hashing;
    $request->signature = $hash;

    return $hash;
  }

  public function upload( $requests, $verify = true ) {
    // Build HTTP requests.
    $http_requests = array();
    foreach ( $requests as $index => $request ) {
      try {
        // Update timestamp.
        $this->update_timestamp();

        // URL and headers.
        $url = $this->upload_url($request);
        $canonical_headers = $this->canonical_headers($request);
        $common_headers = $this->common_headers($request);
        $headers = array_merge( $common_headers, $canonical_headers );
        $headers = array_merge( $headers, $request->http_headers );

        // Sign to headers.
        $signature = $this->sign( $request, $canonical_headers );
        $headers['Authorization'] = sprintf(
          '%s %s:%s',
          $this->auth_prefix,
          $this->key,
          $signature
        );

        // HTTP request.
        $common_options = array_merge( $this->http_common_options, $request->http_common_options );
        $library_options = array_merge( $this->http_library_options, $request->http_library_options );
        $http_request = new Cloud_Uploader_HTTPMessage( 'PUT', $url, $headers, $request->file_path, $common_options, $library_options );
        $http_requests[$index] = $request->http_request = $http_request;
      } catch ( Exception $exception ) {
        $request->exception = $exception;
      }
    }

    // Send http requests.
    $http_responses = $this->http_user_agent->send( $http_requests );

    foreach ( $requests as $index => $request ) {
      try {
        $http_response = $http_responses[$index];
        $request->http_response = $http_response;

        if ( $http_response->exception ) {
          throw $http_response->exception;
        }

        // Set result URL.
        $request->result_url = Cloud_Uploader_Util::cat_url( $this->url_base(), $request->url_path );
      } catch ( Exception $exception ) {
        $request->exception = $exception;
      }
    }

    // Verify.
    if ( $verify ) {
      $this->verify($requests);
    }
  }
}

/**
 * Uploader implementation for Windows Azure Blob storage.
 *
 * Windows Azure Blob storage is very simular to Amazon S3.
 * So inherited from ACloud_Uploader_AmazonS3.
 * $bucket means Container in Azure storage.
 *
 * @package Cloud_Uploader
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class Cloud_Uploader_AzureBlob extends Cloud_Uploader_AmazonS3 {
  /**
   * API version.
   */
  const api_version = '2011-08-18';

  /**
   * Blob type.
   */
  const blob_type = 'BlockBlob';

  /**
   * Authentication header key name.
   */
  protected $auth_prefix = 'SharedKeyLite';

  /**
   * Canonical header's prefix.
   */
  protected $header_prefix = 'x-ms-';

  /**
   * Access secret is encoded.
   */
  protected $to_decode_secret = true;

  /**
   * Hash algorithm for signature is SHA256.
   */
  protected $hash_algorithm = 'sha256';

  /**
   * Storage domain.
   */
  protected $storage_domain = 'blob.core.windows.net';

  /**
   * Override default_url_base.
   */
  public function default_url_base() {
    return sprintf( 'http://%s.blob.core.windows.net/%s/', $this->key, $this->bucket );
  }

  /**
   * Override upload_url method.
   */
  public function upload_url( $request ) {
    $url = sprintf( '%s://%s.%s', $this->protocol, $this->key, $this->storage_domain );
    $url = Cloud_Uploader_Util::cat_url( $url, $this->bucket, $request->url_path );
    return $url;
  }

  /**
   * Override signature_canonical_resource method.
   */
  public function signature_canonical_resource( &$signature, $request ) {
    $path = Cloud_Uploader_Util::cat_url( $this->key, $this->bucket, $request->url_path );
    $signature []= '/' . $path;
  }

  /**
   * Override canonical_headers method.
   */
  public function canonical_headers( $request ) {
    $headers = array(
      'x-ms-blob-content-type'
                        => $request->content_type,
      'x-ms-blob-type'  => self::blob_type,
      'x-ms-date'       => $this->format_timestamp(),
      'x-ms-version'    => self::api_version
    );
    return $headers;
  }
}
